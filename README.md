# SHARE SERVICE

### Install poetry
```shell
pip install poetry
```
 
### Install the project dependencies
```shell
cd src && poetry install
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```

### Make sure you have installed Redis, Celery and Flower

#### Start Redis
```shell
redis-server
```

#### Start Celery
```shell
celery worker --app=share_service --loglevel=info

```

#### Start Flower

```shell
flower -A share_service --port=5555 --broker=redis://127.0.0.1:6379/0
```

