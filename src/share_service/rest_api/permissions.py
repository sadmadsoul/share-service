from rest_framework import permissions


class BaseCustomPermission(permissions.BasePermission):
    anonymous_actions = ()
    authorized_actions = ("retrieve", "list")

    @staticmethod
    def _is_authenticated(request):
        return request.user and request.user.is_authenticated

    def has_permission(self, request, view):
        return any(
            (
                view.action in self.anonymous_actions and self._is_authenticated(request),
                view.action in self.authorized_actions
                and self._is_authenticated(request),
            )
        )
