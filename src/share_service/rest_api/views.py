from core.models import User
from rest_framework import viewsets
from rest_api.permissions import BaseCustomPermission
from rest_api.serializers import UserSerializer
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response


# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [BaseCustomPermission]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    @swagger_auto_schema(
        operation_id="user_read",
        responses={200: UserSerializer()},
    )
    def list(self, request, *args, **kwargs):
        return Response(self.serializer_class(self.get_object()).data)
