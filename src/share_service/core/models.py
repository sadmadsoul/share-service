import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class StateEnum(models.TextChoices):
    CONFIRMED = "CONFIRMED"
    NOT_CONFIRMED = "NOT_CONFIRMED"


class User(AbstractUser):

    code = models.UUIDField(default=uuid.uuid4(), editable=False)
    is_confirmed = models.CharField(
        max_length=255, choices=StateEnum.choices, default=StateEnum.NOT_CONFIRMED
    )
