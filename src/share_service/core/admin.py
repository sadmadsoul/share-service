from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth import get_user_model

# Register your models here.


@admin.register(get_user_model())
class UserAdmin(ModelAdmin):
    list_display = [
        "username",
        "first_name",
        "last_name",
        "email",
        "is_confirmed",
        "is_superuser",
    ]
    list_filter = ["is_confirmed", "is_superuser"]
